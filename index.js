const express = require("express");

const app = express();

const port = 3000;

app.use(express.json());


// Mock data
let users = [
	{
		username: "johndoe",
		password: "johndoe1234" 
	}
];


// ACTIVITY

// Get Home route
app.get('/home', (req,res) => {
	res.send(`Welcome to home page`)
});

// Get Users route
app.get('/users', (req,res) => {
	res.send(users);
});

// Delete user route
app.delete('/delete-user', (req,res) => {
	users.pop();
	res.send(`User ${req.body.username} has been deleted`);
})


app.listen(port,()=>console.log(`Server is running at port ${port}`));
